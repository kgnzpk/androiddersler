package com.ozan.ozupek;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {



    private EditText etKur1;
    private EditText etKur2;
    private Button btnConvert;

    double kur = 2.68;



    private void findViews() {
        etKur1 = (EditText)findViewById( R.id.etKur1 );
        etKur2 = (EditText)findViewById( R.id.etKur2 );
        btnConvert = (Button)findViewById( R.id.btnConvert );


    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        etKur1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                try{

                    String kur1 = etKur1.getText().toString();
                    double money = Double.parseDouble(kur1);
                    double result = money/kur;
                    String resultString = Double.toString(result);
                    etKur2.setText(resultString);





                }catch (Exception ex)
                {
                    etKur2.setText("Dogru Yaz");

                }



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });









    }



}
