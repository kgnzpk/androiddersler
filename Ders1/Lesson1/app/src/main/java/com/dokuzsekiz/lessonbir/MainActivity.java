package com.dokuzsekiz.lessonbir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends Activity {


    private EditText etUserName;
    private EditText etPassword;
    private Button btnLogin;
    private Button btnSignUp;
    private TextView txStatus;
    private CheckBox cbTest;

    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2015-06-08 19:29:43 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void findViews() {
        etUserName = (EditText)findViewById( R.id.etUserName );
        etPassword = (EditText)findViewById( R.id.etPassword );
        btnLogin = (Button)findViewById( R.id.btnLogin );
        btnSignUp = (Button)findViewById( R.id.btnSignUp );
        txStatus = (TextView)findViewById( R.id.txStatus );
        cbTest = (CheckBox)findViewById( R.id.cbTest );


    }

    /**
     * Handle button click events<br />
     * <br />
     * Auto-created on 2015-06-08 19:29:43 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        findViews();




        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cbTest.isChecked())
                {
                    //Admin

                    Random rnd = new Random();
                    int randm = rnd.nextInt(200);

                     Admin a1 = new Admin(randm);

                    String username = etUserName.getText().toString();
                    String password = etPassword.getText().toString();

                    a1.setName(username);
                    a1.Password = password;


                    Intent i =new Intent(MainActivity.this,SecondActivity.class);

                    startActivity(i);







                }
                else
                {
                    //User


                }



            }
        };

        btnLogin.setOnClickListener(listener);






    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
