package com.thewallapp.ders;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {



    View btnKur;
    Button btnDonen;

    View.OnClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnKur = findViewById(R.id.btnKur);
        btnDonen = (Button)findViewById(R.id.btnDonen);

        getActionBar().hide();
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };


        btnKur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,KurDegistir.class);
                i.putExtra("title","Kur Degistir");
                startActivity(i);
                finish();



            }
        });

        btnDonen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ObjectAnimator animator = ObjectAnimator.ofFloat(btnDonen, "rotation", 270.0f);
                animator.setDuration(500);
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                animator.start();




            }
        });
















    }


    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(MainActivity.this,"Activity Pause Edildi",Toast.LENGTH_LONG).show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast.makeText(MainActivity.this,"Activity Destroy Edildi",Toast.LENGTH_LONG).show();


    }
}
